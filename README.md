# Educational Practice (Term 2)
This is the repository for the educational practice in [GUAP] university
for the MATLAB course (2nd educational term)
of the 09.03.04 educational program ("Program Engineering", 2023 admission).

This repository is for the variant 7.

## Usage
Run `menu` script/function.
Then:
* Enter `demo` for the demo example
* Enter `solve` to solve a real equation
* Enter `h/help` for help message
* Enter `about` to show authors & licensing
* Enter `q/quit` to quit

### Example
```
Welcome to Smart Equation Solver
Enter command, or use 'h' or 'help' for help: demo
Enter equation to solve: 0 = sin(x+pi/3) + .5*x + 2
Enter root bounds in format '[ left, right ]': [-3 -1]

Starting solitions...
Solving via 'fzero'...
 > 'fzero' returned -2.184768 in 0.048772 seconds.
Solving via 'my_fzero'...
 > 'my_fzero' returned -2.184768 in 0.023231 seconds.
Done.

Enter command, or use 'h' or 'help' for help: q
Goodbye!
```


## Licensing
*see: [LICENSE.txt](./LICENSE.txt)*

[BSD-2 License].
Copyright (c) Peter Zaitcev, 2024-2025.

<!-- Links -->
[GUAP]: https://guap.ru
[BSD-2 License]: https://opensource.org/license/bsd-2-clause
