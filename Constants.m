classdef Constants
    properties (Constant)
        Title = "Smart Equation Solver";

        BinarySearchEnabled = true;
        BinarySearchEpsilon = 1e-3;
        BinarySearchMaxIterations = 3;
        
        FineEpsilon = 1e-7;
        FineMaxIterations = 5;
    end
end
