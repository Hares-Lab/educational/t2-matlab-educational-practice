function x = my_fzero(func, interval)
    assert(length(interval) == 2, "Expected interval -- an array of 2 numbers, got %i numbers", length(interval));
    intervalCell = num2cell(interval);
    [ left, right ] = intervalCell{:};
    assert(left < right, "Given interval is not positive length; got: %f, %f", left, right);
    
    yLeft = func(left);
    yRight = func(right);
    assert(yLeft * yRight <= 0, "Expected different sign of function on the given interval.")
    if (yLeft == 0)
        x = left;
        return;
    elseif (yRight == 0)
        x = right;
        return;
    elseif (yLeft > yRight)
        % Swap left & right if function is decreasing
        [ left, right ] = set(right, left);
    end
    
    if (Constants.BinarySearchEnabled)
        [ left, right ] = find_root_bounds(func, left, right);
    end
    x = find_exact_root(func, left, right);
end
function [a, b] = set(a, b)
end

function [ x1, x2 ] = find_root_bounds(f, x1, x2)
    i = 0;
    while (abs(x2 - x1) >= Constants.BinarySearchEpsilon && i < Constants.FineMaxIterations)
        x = (x1 + x2) / 2;
        y = f(x);
        if (y > 0)
            x2 = x;
        else
            x1 = x;
        end

        i = i + 1;
    end
end

function x = find_exact_root(f, x, x0)
    i = 0;
	fx0 = f(x0);

    while (abs(x - x0) >= Constants.FineEpsilon && i < Constants.FineMaxIterations)
        x = x0 - fx0 * (x - x0) / (f(x) - fx0);
        i = i + 1;
    end
end
