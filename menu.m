
function menu(arg)
    menu_init();
    while (true)
        if (exist('arg','var') && ~isempty(arg))
            cmd = menu_readCommand(arg);
            arg = [ ];
        else
            cmd = menu_readCommand();
        end
        % fprintf("Command: %s\n", cmd)
        switch (cmd)
            case "solve"
                menu_solve();
            case "demo"
                menu_getDemo();
            case "help"
                menu_getHelp();
            case "about"
                menu_getAbout();
            case "exit"
                fprintf("Goodbye!\n");
                clear();
                close all;
                return
        end
        
        fprintf("\n");
    end
end

function menu_init()
    clc();
    clear();
    fprintf("Welcome to %s\n", Constants.Title);
end

function menu_error(errorMsg)
    cprintf('Errors', "Error: %s\n", errorMsg);
end

function cmd = menu_readCommand(arg)
    while (true)
        if (exist('arg','var') && ~isempty(arg))
            text = arg;
            arg = "";
        else
            text = lower(input("Enter command, or use 'h' or 'help' for help: ", 's'));
        end

        if (text == "")
        elseif (ismember(text, [ "easter", "egg" ]))
            fprintf("This are no easter eggs inside this program.\n");
        elseif (ismember(text, [ "s", "e", "solve", "enter", "1" ]))
            cmd = "solve";
            return
        elseif (ismember(text, [ "d", "demo", "2" ]))
            cmd = "demo";
            return
        elseif (ismember(text, [ "h", "m", "?", "help", "menu", "usage", "3" ]))
            cmd = "help";
            return
        elseif (ismember(text, [ "about", "license", "licence", "4" ]))
            cmd = "about";
            return
        elseif (ismember(text, [ "q", "exit", "quit", "5" ]))
            cmd = "exit";
            return
        else
            menu_error(sprintf("Invalid command: '%s'", text))
        end
    end
end

function res = menu_readYN(prompt)
    while (true)
        text = lower(input(sprintf("%s (Y/n) ", prompt), 's'));
        if (text == "")
            res = true;
            return;
        elseif (ismember(text, [ "y", "yes", "ok" ]))
            res = true;
            return;
        elseif (ismember(text, [ "n", "no" ]))
            res = false;
            return;
        end
    end
end

function menu_makePlot(f, interval)
    figure;
    hold on;
    
    title("Plot of function");
    xlabel('x');
    ylabel('y');
    
    fplot(f, interval, 'b', 'LineWidth', 1.5);
    xline(0);
    yline(0);
    
    fStr = func2str(f);
    legend(sprintf("f(x) = %s", fStr(5:end)));
    
    grid on;
    hold off;
end

function f = menu_solve_readFunc()
    while (true)
        func = input("Enter equation to solve: 0 = ", 's');
        try
            f = str2func(['@(x) ' func]);
            f(0);
            return;
        catch err
            menu_error(sprintf("Invalid input: '%s'\n%s: %s", func, err.identifier, err.message))
        end
    end
end

function interval = menu_solve_readInterval(prompt)
    while (true)
        try
            interval_str = input(sprintf("%s in format 'left, right': ", prompt), 's');
            interval = str2num(interval_str); %#ok<ST2NM>
            if (length(interval) == 2 && isa(interval, "numeric"))
                return
            elseif (interval_str ~= "")
                menu_error(sprintf("Invalid input: '%s'", interval_str))
            end
        catch err
            menu_error(sprintf("Invalid input: '%s'\n%s: %s", interval_str, err.identifier, err.message))
            continue
        end
    end
end

function menu_solve()
    f = menu_solve_readFunc();
    if (menu_readYN("Do you need plot?"))
        plot_interval = menu_solve_readInterval("Enter plot interval");
        menu_makePlot(f, plot_interval);
    end
    interval = menu_solve_readInterval("Enter root bounds");
    close all;
    
    menu_solve_act(f, interval);
end

function menu_solve_act(f, interval)
    fprintf("\nStarting solitions...\n");
    menu_solve_solver('fzero', @fzero, f, interval);
    menu_solve_solver('my_fzero', @my_fzero, f, interval);
    fprintf("Done.\n");
end

function menu_solve_solver(methodName, methodFn, func, interval)
    fprintf("Solving via '%s'...\n", methodName);
    err = false;
    tic
    try result = methodFn(func, interval);
    catch err
    end
    dt = toc;
    if (err == false)
        fprintf(" > '%s' returned %f in %f seconds.\n", methodName, result, dt);
    else
        cprintf('Errors', ...
                " > '%s' FAILED in %f seconds.\n" + ...
                " > Error: %s: %s\n", ...
            methodName, dt, err.identifier, err.message);
    end
end


function menu_getDemo()
    func = 'sin(x+pi/3) + .5*x + 2';
    fprintf("Enter equation to solve: 0 = %s\n", func);
    f = str2func(['@(x) ' func]);
    
    fprintf("Do you need plot? (Y/n) y\n")
    plot_interval = "-10, 15";
    fprintf("Enter plot interval in format 'left, right': %s\n", plot_interval);
    plot_interval = str2num(plot_interval); %#ok<ST2NM>
    menu_makePlot(f, plot_interval);
    
    interval = "-4*pi/3, 0";
    fprintf("Enter root bounds in format 'left, right': %s\n", interval);
    interval = str2num(interval); %#ok<ST2NM>
    
    menu_solve_act(f, interval);
end

function menu_getHelp()
    fprintf("%s\n" + ...
            "Menu:\n" + ...
            "1. Enter an equation to solve (s,e,solve,enter)\n" + ...
            "2. Show the demo example (d,demo)\n" + ...
            "3. Show this message (h,m,?,help,menu,usage)\n" + ...
            "4. Show about & license (about,license,licence)\n" + ...
            "5. Exit menu (q,exit,quit)\n", ...
        Constants.Title);
end

function menu_getAbout()
    fprintf("%s\n" + ...
            "An educational practice task of variant 7.\n" + ...
            "\n" + ...
            "This program uses the following methods of solving:\n" + ...
            " * binary search + secant seach (aka chordes method)\n" + ...
            " * built-in `fzero` function\n" + ...
            "\n" + ...
            "Copyright (c) 2024-2025, Peter Zaitcev\n" + ...
            "Licencsed under BSD-2 license (see LICENSE.txt).\n" + ...
            "GUAP. Z3431.\n", ...
        Constants.Title);
end
